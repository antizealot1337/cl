# CL
[![GoDoc](https://godoc.org/bitbucket.org/antizealot1337/cl?status.svg)](https://godoc.org/bitbucket.org/antizealot1337/cl)

This small and simple library allows the creation of commands and execution of
those commands when a line of input matches the command.

An simple example:
````go
package main

import (
	"bufio"
	"fmt"
	"os"

	"bitbucket.org/antizealot1337/cl"
)

func main() {
	// Create a command set
	cs := cl.NewCommandSet()

	// Add a command
	cs.Add("hello", ExecFn(func(args []string) error {
		fmt.Println("Hello, world!")
		return nil
	}))

	// Create a scanner from standard input
	scanner := bufio.NewScanner(os.Stdin)

  // Scan the input
  for scanner.Scan() {
    // Print something so the user knowns the program expects input
    fmt.Print("> ")

    // Get the line
    line := scanner.Text()

    // Check for a command
    found, err := cs.CheckLine(line)

    // Check for an error
    if err != nil {
      panic(err)
    } //if

    // Check if the command was found
    if !found {
      fmt.Printf("\"%s\" does not contain a command.\n", line)
    } //if
  } //for
} //main
````

The output would be:
````
> nocmd
"nocmd" does not contain a command.
> hello
Hello, World!
>
````

With the new ```Process``` function it can be simplified to:
````go
package main

import (
	"fmt"
	"os"

	"bitbucket.org/antizealot1337/cl"
)

func main() {
	// Create a command set
	cs := cl.NewCommandSet()

	// Add a command
	cs.Add("hello", ExecFn(func(args []string) error {
		fmt.Println("Hello, world!")
		return nil
	}))

	// Process the line
	if err := cl.Process("> ", cs, os.Stdin); err != nil {
		painc(err)
	} //if
} //main
````

## A few things to note

* Commands are case sensitive. The commands "go", "Go", "gO", and "GO" are all
different commands. This may change in the future.
* Commands must be typed in full. A command named "compile" cannot be called
using input "comp". This may change in the future.
* It is up to the cl.Executor to check for args. The cl.CommandSet does not do
any pre-checking for specific arguments.

## Licensing

All code in this repository is licensed under the terms of the MIT license
unless other wise specified. View the LICENSE file for terms of the license.

Copyright 2016, 2017
