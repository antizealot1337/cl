package cl

import (
	"strings"

	"bitbucket.org/antizealot1337/cmdparse"
)

// CommandSet is a set of commands.
type CommandSet struct {
	cmds     map[string]Executor
	partEval bool
} //strutct

// NewCommandSet creates a new CommandSet. The commands can either be determined
// strictly or partially from the command line via the provided bool.
func NewCommandSet(partial bool) *CommandSet {
	return &CommandSet{
		cmds:     make(map[string]Executor, 0),
		partEval: partial,
	}
} //NewCommandSet

// Add an Executor to the CommandSet.
func (c *CommandSet) Add(command string, action Executor) {
	// Add the action as a command
	c.cmds[command] = action
} //func

// CheckLine checks if the can be executed. If not false is returned. If the
// line is associated with a command the command will be executed and true
// returned. If the command encounters an error or parsing the line results in
// and error, it will be returned.
func (c CommandSet) CheckLine(line string) (bool, error) {
	// Break the line
	args, err := cmdparse.Line(line)

	// Check for an error
	if err != nil {
		return false, err
	} //if

	// Check the number of arguments to be sure
	if len(args) == 0 {
		return false, nil
	} //if

	cmd, args := args[0], args[1:]

	// Check if the command determination should be partial or complete
	if c.partEval {
		// Loop through the commands and their keys
		for key, exec := range c.cmds {
			// Check it the command partially matches the provided
			if strings.HasPrefix(key, cmd) {
				return true, exec.Exec(args)
			} //if
		} //for

		return false, nil
	} //if

	// Check if the command set contains a command with the name.
	exec, ok := c.cmds[cmd]

	// Check if the command was found
	if !ok {
		return false, nil
	} //if

	// Return true and any error from the exec.
	return true, exec.Exec(args)
} //CheckLine
