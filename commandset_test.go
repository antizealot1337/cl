package cl

import (
	"errors"
	"testing"
)

func TestNewCommandSet(t *testing.T) {
	// Create a command set
	cs := NewCommandSet(true)

	// Check the command set
	if cs == nil {
		t.Error("Expected a command set to be created")
	} //if

	// Make sure the map isn't empty
	if cs.cmds == nil {
		t.Error("Expected the map of commands to be initialized")
	} //if

	// Make sure partial eval is set to true
	if !cs.partEval {
		t.Error("Expected partial eval to be true")
	} //if
} //func

func TestCommandSetAdd(t *testing.T) {
	// Create a command set
	cs := CommandSet{
		cmds: map[string]Executor{},
	}

	// Some values for the command set
	cmd, exec := "cmd", func(args []string) error {
		return nil
	}

	// Add the command
	cs.Add(cmd, ExecFn(exec))

	// Make sure the command was added
	if expected, actual := 1, len(cs.cmds); actual != expected {
		t.Errorf("Expected the size of the command set to be %d but was %d",
			expected, actual)
	} //if
} //func

func TestCommandSetCheckLine(t *testing.T) {
	// Create a command set
	cs, changed := CommandSet{
		cmds: map[string]Executor{},
	}, false

	// A command for the command set
	cmd, exec := "change", func(args []string) error {
		changed = true
		return nil
	}

	// Add the command
	cs.Add(cmd, ExecFn(exec))

	// Another command for the command set
	cmd, exec = "error", func(args []string) error {
		return errors.New("")
	}

	// Add the command
	cs.Add(cmd, ExecFn(exec))

	// Make sure an empty line returns nothing
	found, err := cs.CheckLine("")

	// Check if found
	if err != nil {
		t.Error("Unexpected error:", err)
	} else if found {
		t.Error("Did not expect an empty line to find a command")
	} //if

	// Make sure there this line has no command
	found, err = cs.CheckLine("nothing")

	// Check the error and found
	if err != nil {
		t.Error("Unexpected error", err.Error())
	} else if found {
		t.Error("Did not expected line to find a command")
	} //if

	// Check the command "change"
	found, err = cs.CheckLine("change")

	// Check the error, found, and switched
	if err != nil {
		t.Error("Unexpected error", err.Error())
	} else if !found {
		t.Error("Expected line to find a command")
	} else if !changed {
		t.Error("Expected the command to have been executed")
	} //if

	// Check the command "error"
	found, err = cs.CheckLine("error")

	// Check the error and found
	if err == nil {
		t.Error("Expected and error")
	} else if !found {
		t.Error("Expected the line to find a command")
	} //if
} //TestCommandSetCheckLine
