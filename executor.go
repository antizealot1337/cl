package cl

// Executor represents a command that can be executed.
type Executor interface {
	Exec(args []string) error
} //interface

// ExecFn is a function that implements the Executor interface.
type ExecFn func(args []string) error

// Exec executes the function.
func (e ExecFn) Exec(args []string) error {
	return e(args)
} //Exec
