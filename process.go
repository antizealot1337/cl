package cl

import (
	"bufio"
	"fmt"
	"io"
)

// Process is a utility method that will read lines from a Reader and provices
// it to a CommandSet. Errors reported from the Executors in the CommandSet are
// expected to be fatal.
func Process(prompt string, c CommandSet, in io.Reader) error {
	// Create a scanner
	scanner := bufio.NewScanner(in)

	// Show the prompt
	fmt.Print(prompt)

	// The input loop
	for scanner.Scan() {
		// Read the line
		line := scanner.Text()

		// Check the line
		found, err := c.CheckLine(line)

		// Check for the error
		if err != nil {
			return err
		} //if

		// Check if the command was found
		if !found {
			fmt.Printf(`Command not found: %s`, line)
		} //if

		// Write the prompt
		fmt.Print(prompt)
	} //for

	return nil
} //func
